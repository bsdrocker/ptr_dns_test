# DNS PTR Test

This is my git repo for the testing I was doing with DNS PTR records in relation to traceroutes. I built this environment using Cumulus VX and Vagrant. Vagrant must be installed before you can use this environment. Find more about Vagrant at vagrantup.com.

To use this environment, clone or download this repo and issue the following command:

```
$ vagrant up
```

You can then SSH into the individual systems by doing:

```
$ vagrant ssh spine1
$ vagrant ssh dns1
```

Below is the network diagram for this small network:

![alt text](diagram.jpg "Network Diagram")
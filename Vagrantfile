Vagrant.configure(2) do |config|

  config.vm.define "spine1" do |spine1|
    spine1.vm.box = "CumulusCommunity/cumulus-vx"

    # Internal network for swp* interfaces
    spine1.vm.network "private_network", virtualbox__intnet: "spn1_spn2_0", auto_config: false
    spine1.vm.network "private_network", virtualbox__intnet: "spn1_spn2_1", auto_config: false
    spine1.vm.network "private_network", virtualbox__intnet: "spn1_lsw1", auto_config: false
    spine1.vm.network "private_network", virtualbox__intnet: "spn1_lsw2", auto_config: false
  end

  config.vm.define "leaf1" do |leaf1|
    leaf1.vm.box = "CumulusCommunity/cumulus-vx"

    # Internal network for swp* interfaces
    leaf1.vm.network "private_network", virtualbox__intnet: "spn1_lsw1", auto_config: false
    leaf1.vm.network "private_network", virtualbox__intnet: "spn2_lsw1", auto_config: false
    leaf1.vm.network "private_network", virtualbox__intnet: "lsw1_lsw2_0", auto_config: false
    leaf1.vm.network "private_network", virtualbox__intnet: "lsw1_lsw2_1", auto_config: false
    leaf1.vm.network "private_network", virtualbox__intnet: "lsw1_dns1", auto_config: false
  
    leaf1.vm.provider "virtualbox" do |vbox|
      vbox.customize ['modifyvm', :id, '--nicpromisc2', 'allow-all']
      vbox.customize ['modifyvm', :id, '--nicpromisc3', 'allow-all']
      vbox.customize ['modifyvm', :id, '--nicpromisc4', 'allow-all']
      vbox.customize ['modifyvm', :id, '--nicpromisc5', 'allow-all']
      vbox.customize ['modifyvm', :id, '--nicpromisc6', 'allow-all']
    end
  end

  config.vm.define "leaf2" do |leaf2|
    leaf2.vm.box = "CumulusCommunity/cumulus-vx"

    # Internal network for swp* interfaces
    leaf2.vm.network "private_network", virtualbox__intnet: "spn1_lsw2", auto_config: false
    leaf2.vm.network "private_network", virtualbox__intnet: "spn2_lsw2", auto_config: false
    leaf2.vm.network "private_network", virtualbox__intnet: "lsw1_lsw2_0", auto_config: false
    leaf2.vm.network "private_network", virtualbox__intnet: "lsw1_lsw2_1", auto_config: false
    leaf2.vm.network "private_network", virtualbox__intnet: "lsw2_host1", auto_config: false
    
    leaf2.vm.provider "virtualbox" do |vbox|
      vbox.customize ['modifyvm', :id, '--nicpromisc2', 'allow-all']
      vbox.customize ['modifyvm', :id, '--nicpromisc3', 'allow-all']
      vbox.customize ['modifyvm', :id, '--nicpromisc4', 'allow-all']
      vbox.customize ['modifyvm', :id, '--nicpromisc5', 'allow-all']
      vbox.customize ['modifyvm', :id, '--nicpromisc6', 'allow-all']
    end
  end

  config.vm.define "dns1" do |dns1|
    dns1.vm.box = "ubuntu/xenial64"

    # eth1 for dns1
    dns1.vm.network "private_network", virtualbox__intnet: "lsw1_dns1", auto_config: false

    # Fixes "stdin: is not a tty" and "mesg: ttyname failed : Inappropriate ioctl for device"  messages --> https://github.com/mitchellh/vagrant/issues/1673
    dns1.vm.provision :shell , inline: "(grep -q 'mesg n' /root/.profile 2>/dev/null && sed -i '/mesg n/d' /root/.profile  2>/dev/null && echo 'Ignore the previous error, fixing this now...') || true;"

    # Install bind, traceroute, lldpd
    dns1.vm.provision :shell, inline: "sudo apt-get update"
    dns1.vm.provision :shell, inline: "sudo apt-get install bind9 traceroute lldpd -y"

    # Network configuration
    dns1.vm.provision "file", source: "configs/dns1/50-cloud-init.cfg", destination: "/home/ubuntu/50-cloud-init.cfg"
    dns1.vm.provision "file", source: "configs/dns1/99-disable-network-config.cfg", destination: "/home/ubuntu/99-disabled-network-config.cfg"
    dns1.vm.provision :shell, inline: "sudo cp /home/ubuntu/50-cloud-init.cfg /etc/network/interfaces.d/"
    dns1.vm.provision :shell, inline: "sudo cp /home/ubuntu/99-disabled-network-config.cfg /etc/cloud/cloud.cfg.d/"
    dns1.vm.provision :shell, inline: "sudo systemctl restart networking.service"

    # Bind9 configuration
    dns1.vm.provision "file", source: "dns_zone/db.168.192", destination: "/home/ubuntu/db.168.192"
    dns1.vm.provision "file", source: "dns_zone/db.dnstest.local", destination: "/home/ubuntu/db.dnstest.local"
    dns1.vm.provision "file", source: "dns_zone/named.conf.default-zones", destination: "/home/ubuntu/named.conf.default-zones"
    dns1.vm.provision :shell, inline: "sudo cp /home/ubuntu/db.168.192 /etc/bind/"
    dns1.vm.provision :shell, inline: "sudo cp /home/ubuntu/db.dnstest.local /etc/bind/"
    dns1.vm.provision :shell, inline: "sudo cp /home/ubuntu/named.conf.default-zones /etc/bind/"
    dns1.vm.provision :shell, inline: "sudo systemctl restart bind9.service"

    # Set adapter to Promisc and Allow-All
    dns1.vm.provider "virtualbox" do |vbox|
      vbox.customize ['modifyvm', :id, '--nicpromisc2', 'allow-all']
    end
  end

  config.vm.define "host1" do |host1|
    host1.vm.box = "ubuntu/xenial64"

    # eth1 for host1
    host1.vm.network "private_network", virtualbox__intnet: "lsw2_host1", auto_config: false

    # Fixes "stdin: is not a tty" and "mesg: ttyname failed : Inappropriate ioctl for device"  messages --> https://github.com/mitchellh/vagrant/issues/1673
    host1.vm.provision :shell , inline: "(grep -q 'mesg n' /root/.profile 2>/dev/null && sed -i '/mesg n/d' /root/.profile  2>/dev/null && echo 'Ignore the previous error, fixing this now...') || true;"

    # Install traceroute, lldpd
    host1.vm.provision :shell, inline: "sudo apt-get update"
    host1.vm.provision :shell, inline: "sudo apt-get install traceroute lldpd -y"

    # Network configuration
    host1.vm.provision "file", source: "configs/host1/50-cloud-init.cfg", destination: "/home/ubuntu/50-cloud-init.cfg"
    host1.vm.provision "file", source: "configs/host1/99-disable-network-config.cfg", destination: "/home/ubuntu/99-disabled-network-config.cfg"
    host1.vm.provision :shell, inline: "sudo cp /home/ubuntu/50-cloud-init.cfg /etc/network/interfaces.d/"
    host1.vm.provision :shell, inline: "sudo cp /home/ubuntu/99-disabled-network-config.cfg /etc/cloud/cloud.cfg.d/"
    host1.vm.provision :shell, inline: "sudo resolvconf -d enp0s3.dhclient"
    host1.vm.provision :shell, inline: "sudo systemctl restart networking.service"

    host1.vm.provider "virtualbox" do |vbox|
      vbox.customize ['modifyvm', :id, '--nicpromisc2', 'allow-all']
    end
  end

end